package com.kuding.text;

import com.kuding.pojos.servicemonitor.ServiceCheckNotice;

@FunctionalInterface
public interface ServiceMonitorResolver extends NoticeTextResolver<ServiceCheckNotice> {

}
